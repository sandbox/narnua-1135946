 
-- DESCRIPTION --

Drupal wrapper for Thingiview - an in-browser 3D file viewer powered by WebGL.
https://github.com/tbuser/thingiview.js

-- INSTALLATION --

Install as usual

Viewer will show up on any node that has .stl or .obj files as filefields. 

-- NOTES --

* Viewer initialization might fail. It's called within node load action, which on one of the
test sites where node load was called multiple times on the page to display sidebar blocks
caused the viewer not initialize.
 
* The viewer might show up on node teaser when multiple nodes are being viewed (e.g. front page. 
As above, this happened on a test site with a lot going on, should work on clean Drupal 6 installations. 
Current quick fix is to check up node #context variable to determine teaser or full node status. 

* Interface for including the viewer has not been decided yet: currently it will be loaded up for any 
nodes containing .stl or .obj in filefields